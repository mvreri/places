(ns places.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.json :as middleware]
            [places.query :as query]
            [cheshire.core :as cheshire]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clj-http.client :as httpclient]
            [places.config :as config]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clj-time.core :as t]
            [clj-time.periodic :refer [periodic-seq]])
  )

(defroutes app-routes


           (POST "/api/places" request
             (let [body (:body request)]
               (timbre/info body)
               (timbre/info "<<<<<<<<" (:request body))

               (let  [response (httpclient/get (str "https://api.foursquare.com/v2/venues/search?ll=" (get-in body ["coordinates"]) "&intent=browse&radius="(get-in body ["radius"]) "&query="(get-in body ["query"]) "&client_id=ZZDEGQWGLED4Z0SHKF0OUI2F11O1TM4RAJ5YWQQ51ZWY4JTC&client_secret=JUYQWHGNVDFLBAXUERMIDM0A0S44D55HEBRJJ1X3MZZVOKUE&v="(.format (java.text.SimpleDateFormat. "yyyyMMdd") (new java.util.Date)) )  )]


                 (timbre/info "Response from 4sq" (:body response) )


                 ;(println "------->>>>>" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" ])))
                 ;(println "Name " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "name"])))
                 ;(println "Twitter " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "contact" "twitter"])))
                 ;(println "Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "address"])))
                 ;(println "Formatted Address " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "formattedaddress"])))
                 ;(println "Country Code" (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "cc"])))
                 ; (println "Distance from here: " (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" 0 "location" "distance"])))


                 (for   [n (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues" ]))]
                   (println ";;;;;;;;;;;;;;;;;;" (:name n) "," (:twitter (:contact n)) "," (:address (:location n)) "-"(:crossStreet (:location n)) ", Distance:" (:distance (:location n))", Coordinates " (str (:lat (:location n)) "," (:lng (:location n))) ", Category" (:name (:categories n)))

                        )
                 #_(for   [q (clojure.walk/keywordize-keys (get-in (json/read-str (:body response) ) ["response" "venues"  0 "categories"]))]
                   (println ";;;;;;;;;;;;;;;;;; , Category" (:name q))

                        )



                 )
               )
             )









           (route/resources "/")
           (route/not-found "Not Found"))

(def app
  (-> app-routes
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)
      (wrap-defaults app-routes)))
